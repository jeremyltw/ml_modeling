import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import csv

stepDownPath = os.path.join(os.path.pardir,os.path.pardir)
rawDataPath = os.path.join(stepDownPath,"data\\raw\\data.xlsx")
interimDataPath = os.path.join(stepDownPath,"data\\interim")

xl = pd.ExcelFile(rawDataPath)

# parsing the data into a dataframe

df = xl.parse("Raw")


counter = 0
J = 10 # initializing to a value that's larger than the while condition

# initializing thetas
theta_0 =0
theta_1 = 0

source_rows = len(df.index)

alpha_0 = 0.01 # learning rate for theta 0
alpha_1 = 0.01 # learning rate for theta 1

list_output = list()

while abs(J) > 0.001:
    sumHypError0 = 0
    sumHypError1 = 0

    for row in df.itertuples():
        hyp0 = theta_0 + theta_1 * row.X
        sumHypError0+= hyp0 - row.Y
        sumHypError1+= (hyp0-row.Y) * row.X
    
    J = (sumHypError0 ** 2) * 0.5 / source_rows

    list_output.append((counter,theta_0,theta_1,J,sumHypError0,sumHypError1))
    # df_output[counter] = [counter,theta_0,theta_1,J,sumHypError0,sumHypError1]

    theta_0 = theta_0 - alpha_0 / source_rows * sumHypError0
    theta_1 = theta_1 - alpha_1 / source_rows * sumHypError1
    
    if counter == 5000 :#iteration limit
        break

    counter +=1

with open(os.path.join(interimDataPath,"trained_values.csv"),"w") as out:
    csv_out = csv.writer(out)
    csv_out.writerow(["iteration","theta0","theta1","HypError0","HypError1"])
    csv_out.writerows(row for row in list_output)

print("Completed!")
