import os
import csv
import time

# stepDownPath = os.path.join(os.path.pardir,os.path.pardir)
stepDownPath = os.path.pardir

# rawDataPath = os.path.join(stepDownPath,"data\\raw\\data.xlsx")
interimDataPath = os.path.join(stepDownPath,"data\\interim")

# xl = pd.ExcelFile(rawDataPath)

# # parsing the data into a dataframe

# df = xl.parse("Raw")

def optimizelrvariables(df,theta_0=0,theta_1=0,alpha_0 = 0.01,alpha_1 = 0.01,threshold=0.001,writecalculationtofile=True):
    start_time = time.time()
    counter = 0

    source_rows = len(df.index)

    list_output = list()
    
    diff = 10 # initializing to a value that's larger than the while condition

    while abs(diff) > threshold:

        sumHypError0 = 0
        sumHypError1 = 0

        for row in df.itertuples():
            hyp0 = theta_0 + theta_1 * row.X
            sumHypError0+= hyp0 - row.Y
            sumHypError1+= (hyp0-row.Y) * row.X
        
        J = (sumHypError0 ** 2) * 0.5 / source_rows

        list_output.append((counter,theta_0,theta_1,J,sumHypError0,sumHypError1))
        # df_output[counter] = [counter,theta_0,theta_1,J,sumHypError0,sumHypError1]

        diff_0 = sumHypError0 / source_rows
        diff_1 = sumHypError1 / source_rows

        theta_0 = theta_0 - alpha_0 * diff_0
        theta_1 = theta_1 - alpha_1 * diff_1

        if(diff_1>diff_0):
            diff = diff_1
        else:
            diff=diff_0

        if counter == 5000 :#iteration limit
            print("Convergence may not be achieved as it has reached an iteration limit of {}".format(counter))
            break

        counter +=1

    if writecalculationtofile:
        with open(os.path.join(interimDataPath,"trained_values.csv"),"w") as out:
            csv_out = csv.writer(out)
            csv_out.writerow(["iteration","theta0","theta1","HypError0","HypError1"])
            csv_out.writerows(row for row in list_output)

    end_time = time.time()
    return ((theta_0,theta_1),{"theta_0": theta_0,"theta_1":theta_1,"J":J,"diff_0":diff_0,"diff_1":diff_1,"iterations":counter,"time_taken":end_time-start_time})


